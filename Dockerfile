FROM centos:7
MAINTAINER "Tristan Lins" <tristan@lins.io>

COPY install-temurin.sh /bin

# Install required tools
RUN set -x \
    && yum install -y git curl \
    && yum clean all \
    && rm -rf /var/cache/yum

# Install Java JDK
RUN set -x \
    && install-temurin.sh -f 8 -o linux -a x64 \
    && install-temurin.sh -f 11 -o linux -a x64 \
    && install-temurin.sh -f 17 -o linux -a x64 \
    && install-temurin.sh -f 21 -o linux -a x64

# Setup binaries and java home path
ENV PATH /opt/java11/bin:$PATH
ENV HOME /tmp
ENV JAVA_HOME /opt/java11

# Check java and gradle work properly
RUN set -x \
    && java -version
# Run configuration
WORKDIR "/jsass"
